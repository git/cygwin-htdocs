<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="../style.css" />
  <link rel="stylesheet" type="text/css" href="style.css" />
  <title>Cygwin Gold Stars</title>
</head>

<body>
<!--#include virtual="../navbar.html" -->
<div id="main">
<!--#include virtual="../top.html" -->
<h1>Cygwin Gold Star Awards</h1>
<p>
<a name="about"></a>
This page is dedicated to the Cygwin Gold Star award.  The award is given
to people who have pleased a Cygwin project lead
by their actions or contributions.  Each star is a link detailing the
circumstances in which it was awarded.
</p>

<h2 class="cartouche">Distinguished recipients</h2>

__LIST_OF_AWARDS__

</div>
</body>
</html>
